-- EXERCICE 1:
-- les requêtes classiques
-- avec IN 
-- avec EXISTS
-- avec minus etc ...

-- EX2 

-- 1

SELECT idPan, nomPan FROM PANEL
WHERE idPan NOT IN (SELECT idPan FROM CONSTITUER 
WHERE numSond IN (SELECT numSond FROM SONDE WHERE nomsond='DJARA'
AND prenomSond='Louane'));

-- 2 

SELECT DISTINCT prenomSond FROM SONDE
WHERE prenomSond NOT IN 
(SELECT prenomSond FROM SONDE WHERE idC IN (SELECT idC
FROM CARACTERISTIQUE WHERE idTR = '1')) AND prenomSond LIKE 'A%';

-- 3

SELECT nomPan FROM PANEL
WHERE idPan NOT IN (SELECT idPan FROM CONSTITUER
WHERE numSond IN (SELECT numSond FROM SONDE
WHERE DATEDIFF(CURDATE(), dateNaisSond)/365 >= 60))

-- 4 

SELECT intituleCat FROM CATEGORIE
WHERE idCat IN (SELECT idCat FROM CARACTERISTIQUE
WHERE idC IN (SELECT idC FROM SONDE 
WHERE YEAR(dateNaisSond) = 1977));

-- 5 

SELECT nomSond, prenomSond FROM SONDE WHERE
numSond IN (SELECT DISTINCT s1.numSond 
FROM CONSTITUER s1, CONSTITUER s2
WHERE s1.numSond = s2.numSond AND s1.idPan = '1' 
AND s2.idPan = '3') 
AND YEAR(dateNaisSond) = 1999;

-- 6 

SELECT s1.nomSond, s1.prenomSond, s2.nomSond, s2.prenomSond
FROM SONDE s1, SONDE s2
WHERE s1.dateNaisSond = s2.dateNaisSond AND YEAR(s1.dateNaisSond) = 1977 AND s1.numSond > s2.numSond;