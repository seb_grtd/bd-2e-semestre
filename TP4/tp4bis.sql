SELECT numetu, nometu, prenometu, IFNULL(count(numco),0) as nb_cours
FROM ETUDIANT
NATURAL LEFT JOIN SUIVRE
group by numetu
ORDER BY numetu;

SELECT nomco, IFNULL(sum(coefev),0) FROM COURS
NATURAL LEFT JOIN EVALUATION
group by numco
ORDER BY nomco;

SELECT numetu, nometu, prenometu, sum(note*coefev)/sum(coefev) as moyenne
FROM ETUDIANT
NATURAL JOIN OBTENIR
NATURAL JOIN EVALUATION
group by numetu
HAVING moyenne > 10;

SELECT c.nomco, IFNULL(count(numev),0) as nb_dev FROM COURS c
LEFT JOIN EVALUATION e ON (YEAR(e.dateev) = 2023 and e.numco = c.numco)
GROUP BY c.numco
ORDER BY c.nomco;

SELECT numev, nomev, sum(note*coefev)/sum(coefev) as moyenne
FROM ETUDIANT 
LEFT JOIN OBTENIR using (numetu)
NATURAL JOIN EVALUATION
group by numev
HAVING moyenne > 12;

SELECT * FROM EVALUATION
LEFT JOIN OBTENIR USING (numev)
LEFT JOIN ETUDIANT using(numetu);